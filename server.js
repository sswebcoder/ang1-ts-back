var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3');
var db = new sqlite3.Database('./db.sqlite');
var port = process.env.PORT || 3002;
var router = express.Router();
// Нужно сгенерировать 2 запроса для выборки с поиском и без, но времени мало.
var sqlQuery = 'SELECT id, title, briefly, content, image, STRFTIME("%d.%m.%Y", date) as date';
sqlQuery += ' FROM (SELECT * FROM news where title like "%"||(?)||"%" OR content like "%"||(?)||"%" ORDER BY DATE(date) DESC)';
sqlQuery += ' LIMIT (?) OFFSET (?)';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//db.on('trace', function() {console.log(arguments); });

router.route('/news').get(function(req, res) {
    var start = req.query.start || 0;
    var limit = req.query.limit || 2;
    var search = req.query.search || '';
    db.all(sqlQuery, [search, search, limit, start], function(err, rows) {
        res.json(rows);   
    });
});

router.route('/news/:id_news').get(function(req, res) {
    var id_news = req.params.id_news;
    db.get('SELECT * FROM news WHERE id=(?)', [id_news], function(err, row) {
        if (row) {
            res.json(row);
        } else {
            res.status(404).json({message: "Not Found"});
        }
    });
});

app.use('/api', router);

app.listen(port, function () {
  console.log('Server listening on port ' + port);
});
